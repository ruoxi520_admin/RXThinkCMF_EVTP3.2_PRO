<?php
// +----------------------------------------------------------------------
// | RXThinkCMF敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2023 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace Admin\Controller;


use Admin\Model\UserModel;
use Admin\Service\UserPermissionService;
use http\Client\Request;
use Think\Controller;

class BaseController extends Controller
{
    // 用户ID
    protected $userId;
    // 用户信息
    protected $userInfo;
    // 模型
    protected $model;
    // 服务
    protected $service;

    /**
     * 初始化
     * @author 牧羊人
     * @since 2021/1/17
     */
    protected function _initialize()
    {
        // 跨域处理
        $this->cross();
        // 登录校验
        $this->checkLogin();
        // 初始化配置
        $this->initConfig();
    }

    /**
     * 跨域处理
     * @author 牧羊人
     * @since 2022/3/1
     */
    public function cross()
    {
        header('Content-Type:application/json; charset=utf-8');
        header("Access-Control-Allow-Origin: *");
        // 指定域名放行
//        header('Access-Control-Allow-Origin:前端域名地址');
//        header('Access-Control-Max-Age:86400');
        header('Access-Control-Allow-Methods:OPTIONS,POST,PUT,DELETE');
        header('Access-Control-Allow-Headers:*');
    }

    /**
     * 登录验证
     * @author 牧羊人
     * @since 2022/3/19
     */
    public function checkLogin()
    {
        $noLoginActs = array("Login");
        if (!in_array(CONTROLLER_NAME, $noLoginActs)) {
            // 获取头信息
            $headers = apache_request_headers();
            // 获取Token
            $token = $headers['Authorization'];
            if ($token && strpos($token, 'Bearer ') !== false) {
                $token = str_replace("Bearer ", null, $token);
                // JWT解密token
                $jwt = new \Jwt();
                $userId = $jwt->verifyToken($token);
                if (!$userId) {
                    // token解析失败跳转登录页面
                    $this->ajaxReturn(message("请登录", false, null, 401));
                }
                $this->userId = $userId;
            } else {
                // 跳转至登录页面
                $this->ajaxReturn(message("请登录", false, null, 401));
            }
        }
    }

    /**
     * 初始化配置
     * @author 牧羊人
     * @since 2021/1/17
     */
    private function initConfig()
    {
        // 网站全称
        $this->assign("siteName", C('SITE_NAME'));
        // 网站简称
        $this->assign("nickName", C('NICK_NAME'));
        // 版本号
        $this->assign('version', C('VERSION'));

        //系统分页参数
        define('PERPAGE', isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 0);
        define('PAGE', isset($_REQUEST['page']) ? $_REQUEST['page'] : 0);

        // 数据表前缀
        define('DB_PREFIX', C('DB_PREFIX'));
        // 数据库名
        define('DB_NAME', C('DB_NAME'));

        //系统应用参数
        define('APP', CONTROLLER_NAME);
        define('ACT', ACTION_NAME);
        $this->assign('module', __MODULE__);
        $this->assign('app', APP);
        $this->assign('act', ACT);
    }

    /**
     * 后台入口
     * @author 牧羊人
     * @since 2021/1/17
     */
    public function index()
    {
        $result = $this->service->getList();
        $this->ajaxReturn($result);
    }

    /**
     * 获取数据详情
     * @return mixed
     * @since 2020/11/11
     * @author 牧羊人
     */
    public function info()
    {
        $result = $this->service->info();
        $this->ajaxReturn($result);
    }

    /**
     * 添加或编辑
     * @author 牧羊人
     * @since 2021/1/17
     */
    public function edit()
    {
        $result = $this->service->edit();
        $this->ajaxReturn($result);
    }

    /**
     * 删除数据
     * @return mixed
     * @since 2020/11/11
     * @author 牧羊人
     */
    public function delete()
    {
        $result = $this->service->delete();
        $this->ajaxReturn($result);
    }

    /**
     * 设置状态
     * @return mixed
     * @since 2020/11/21
     * @author 牧羊人
     */
    public function status()
    {
        $result = $this->service->status();
        $this->ajaxReturn($result);
    }

}