<?php

namespace Admin\Service;

use Admin\Model\UserRoleModel;

/**
 * 用户角色-服务类
 * @author 牧羊人
 * @since 2022/3/3
 */
class UserRoleService extends BaseService
{
    /**
     * 构造函数
     */
    public function __construct()
    {
        $this->model = new UserRoleModel();
    }

    /**
     * 根据用户ID获取角色列表
     * @param $userId 用户ID
     * @return array|false|mixed|string|null
     * @author 牧羊人
     * @since 2022/2/17
     */
    public function getUserRoleList($userId)
    {
        $roleList = $this->model->alias('ur')
            ->join(DB_PREFIX . 'role as r ON ur.role_id=r.id')
            ->where('ur.user_id=' . $userId)
            ->where('ur.user_id=' . $userId . ' and r.status=1 and r.mark=1')
            ->field('r.*')
            ->order('sort asc')
            ->select();
        return $roleList;
    }

}