<?php
// +----------------------------------------------------------------------
// | RXThinkCMF敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2023 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace Admin\Service;

use Admin\Model\CityModel;
use Admin\Model\UserModel;
use Admin\Model\UserRoleModel;

/**
 * 用户-服务类
 * @author 牧羊人
 * @since 2021/1/17
 * Class UserService
 */
class UserService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2021/1/17
     * UserService constructor.
     */
    public function __construct()
    {
        $this->model = new UserModel();
    }

    /**
     * 获取数据列表
     * @return array
     * @since 2022/2/12
     * @author 牧羊人
     */
    public function getList()
    {
        // 参数
        $param = I("request.");

        //查询条件
        $map = [];
        $keywords = trim($param['keywords']);
        if ($keywords) {
            // 真实姓名
            $where['realname'] = array('like', "%{$keywords}%");
            // 手机号
            $where['mobile'] = array('like', "%{$keywords}%");
            // 或查询条件
            $where['_logic'] = 'OR';
            // 复杂查询
            $map['_complex'] = $where;
        }
        return parent::getList($map, "sort asc"); // TODO: Change the autogenerated stub
    }

    /**
     * 添加或编辑
     * @return array
     * @since 2022/2/13
     * @author 牧羊人
     */
    public function edit()
    {
        // 参数
        $data = I('post.');
        // 用户角色
        $roleIds = getter($data, "role_ids");
        unset($data['role_ids']);
        // 用户名
        $username = getter($data, "username");
        // 密码
        $password = getter($data, "password");
        // 用户ID
        $id = getter($data, "id", 0);
        // 添加时设置密码
        if (empty($id)) {
            // 设置密码
            $data['password'] = get_password($password . $username);
            // 用户名重复性验证
            $count = $this->model
                ->where([
                    ['username' => $username],
                    ['mark' => 1]
                ])->count('id');
            if ($count > 0) {
                return message("系统中已存在相同的用户名", false);
            }
        } else {
            // 用户名重复性验证
            $count = $this->model
                ->where([
                    ['username' => $username],
                    ['id' => array('neq', $id)],
                    ['mark' => 1]
                ])
                ->count('id');
            if ($count > 0) {
                return message("系统中已存在相同的用户名", false);
            }
            // 获取用户信息
            $info = $this->model->getInfo($id);
            if (!$info) {
                return message("用户信息不存在", false);
            }
            $data['password'] = $info['password'];
        }

        // 头像处理
        $avatar = getter($data, 'avatar');
        if (!empty($avatar)) {
            if (strpos($avatar, "temp") !== false) {
                $data['avatar'] = save_image($avatar, 'user');
            } else {
                $data['avatar'] = str_replace(IMG_URL, "", $avatar);
            }
        }

        // 出生日期
        if (isset($data['birthday']) && $data['birthday']) {
            $data['birthday'] = strtotime($data['birthday']);
        }

        // 城市数据处理
        $city = isset($data['city']) && !empty($data['city']) ? explode(',', $data['city']) : [];
        if (!empty($city)) {
            $data['province_code'] = $city[0];
            $data['city_code'] = $city[1];
            $data['district_code'] = $city[2];
            unset($data['city']);
        } else {
            $data['province_code'] = 0;
            $data['city_code'] = 0;
            $data['district_code'] = 0;
        }
        // 保存数据
        $error = "";
        $result = $this->model->edit($data, $error);
        if (!$result) {
            return message($error, false);
        }

        // 删除用户整体缓存
        $this->model->cacheDAll();

        // 删除用户角色关系数据
        $userRoleModel = new UserRoleModel();
        $userRoleModel->where("user_id=" . $result)->delete();

        // 创建用户角色关系
        if (!empty($roleIds)) {
            $roleArr = explode(',', $roleIds);
            $totalNum = 0;
            foreach ($roleArr as $val) {
                $userRoleModel = new UserRoleModel();
                if ($userRoleModel->add([
                    'user_id' => $result,
                    'role_id' => $val,
                ])) {
                    $totalNum++;
                }
            }
            if ($totalNum != count($roleArr)) {
                return message("用户角色创建失败", false);
            }
        }
        return message();
    }

    /**
     * 获取用户信息
     * @param $userId 用户ID
     * @return array
     * @author 牧羊人
     * @since 2022/3/3
     */
    public function getUserInfo($userId)
    {
        $userInfo = $this->model->getInfo($userId);
        // 返回参数
        $result = array();
        $result['id'] = $userInfo['id'];
        $result['avatar'] = $userInfo['avatar'];
        $result['realname'] = $userInfo['realname'];
        $result['nickname'] = $userInfo['nickname'];
        $result['gender'] = $userInfo['gender'];
        $result['mobile'] = $userInfo['mobile'];
        $result['email'] = $userInfo['email'];
        $result['address'] = $userInfo['address'];
        $result['intro'] = $userInfo['intro'];
        $result['roles'] = [];
        $result['authorities'] = [];
        // 权限节点列表
        $menuService = new MenuService();
        $permissionList = $menuService->getPermissionFuncList($userId);
        $result['permissionList'] = $permissionList;
        return message("操作成功", true, $result);
    }

    /**
     * 重置密码
     * @return array
     * @since 2021/3/14
     * @author 牧羊人
     */
    public function resetPwd()
    {
        // 获取参数
        $data = I("post.");
        $userId = (int)$data['id'];
        $info = $this->model->getInfo($userId);
        if (!$info) {
            return message('当前用户信息不存在', false);
        }
        $data['password'] = get_password("123456" . $info['username']);
        return parent::edit($data);
    }

}